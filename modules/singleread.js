
const fs = require('fs');


const rf = require('readline');


const singleRead = ( sio, socket, socketUsers )=>{

	const logFilename = socketUsers[socket.id]['filenamePath'];



 	return new Promise(( resolve,reject )=>{


 		fs.stat( logFilename , (err,stat) => {
 				
 				//store startig position for file read	
 				let calculateStartSize = 0;
 				
 				// read file from staring position to eof.
				const rl = rf.createInterface({
			  		input: fs.createReadStream( logFilename , { start : calculateStartSize } ),
			  		crlfDelay: Infinity
				});
				
				

				if( socketUsers.indexOf(socket.id) == -1 ){
					
					const lastNLines = []
					rl.on('line', (line) => {
					
						lastNLines.push(line)
						console.log(lastNLines)
						if (lastNLines.length > 10 ) {
							lastNLines.shift()
						}			

					});

					rl.on('close', ()=> {
						for (arrline of lastNLines) {
							sio.to(`${socket.id}`).emit('streamingdata', arrline);
							lineData = arrline;
						
							if( lineData == "" ){
								reject("Failed to read the file");
							}else{
								resolve( [socketUsers , socket] );
							}
						}
					  
					  	
					})
				}
		});		
	});		
}


const getLastSize = ( socketUsers , socket )=>{

	const logFilename = socketUsers[socket.id]['filenamePath'];

	return new Promise( ( resolve, reject ) =>{

		fs.stat(logFilename,(err,statser)=>{
			let currFileLastSize = statser.size;
			
			if( currFileLastSize == 0 ){
				reject("Failed to read the file");
			}else{
				resolve(currFileLastSize);
			}
		});	
	});
}


const initializeSingleRead = function( sio, socket, socketUsers ){

	const lastfilereadPosition = singleRead( sio, socket, socketUsers )
	.then( (socketUsers )=>{
		return getLastSize( socketUsers[0] , socketUsers[1] );
	},function(error){
		throw(error);
	}).then( (result) => {
		console.log("Last file read position "+result );
		return result;
	}).catch((err)=>{
		console.log("Error while single read"+err);
	});
	return lastfilereadPosition;
}


module.exports = initializeSingleRead;	