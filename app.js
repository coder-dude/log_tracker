
console.log("app started");

const express = require("express");


const fileReader = require("./modules/filereader");


const singleread = require("./modules/singleread");


const listingPort = 3000;


const app = express();


const server = require('http').createServer(app);  


const sio = require("socket.io")(server);

app.use(express.static("."));
app.get("/",(req,res, next) => {
	res.sendFile('index.html');
});


let socketUsers = [];

server.listen(listingPort);


sio.on('connection',( socket )=> {
	
	console.log( "Connection established with socket id : "+socket.id );

	socket.on('disconnect', function () {
        console.log('user disconnected of id '+socket.id);
    });



	socket.on('viewlogs',function( data ){

		socketUsers[socket.id] = new Array();

		socketUsers[socket.id]['filenamePath'] = data.filename ;

		const firstAttemptSize = function( socketUsers,socket ){
			return new Promise( (resolve,reject)=> {
				var lastfilereadPosition = singleread( sio, socket, socketUsers ) ;
				resolve( lastfilereadPosition, socket );
			});
		}


		const continuesRead = function(){
			return new Promise( ( resolve,reject )=> {
				fileReader( sio, socketUsers, socket);
			});
		}

		firstAttemptSize(socketUsers,socket)
		.then( (result)=>{
			socketUsers[socket.id]['location'] = result;
			return ;
		}).then(()=>{
			continuesRead(); 
		});


	});



});

