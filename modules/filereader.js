
const fs = require('fs');


const rf = require('readline');


const reader = ( sio, socketUsers, socket)=>{
	

	let oldsize = 0;


	let shouldWait = false;
	

	const logFilename = socketUsers[socket.id]['filenamePath'];
	
	//This function will start watching file for any updates
	fs.watch( logFilename , function( event ){

		//wait till true and than handle event
		if(shouldWait) return;
		else{
			shouldWait = setTimeout( () => {
			shouldWait = false;	
			}, 100);	
		fs.stat( logFilename , (err,stat) => {
			
			
			let oldsize = socketUsers[socket.id]['location'];
			
			//get the file data from start and end position specified.

			console.log("logfile", logFilename, "old size", oldsize, "statsize", stat)	
			const fsread =  fs.createReadStream(logFilename , { start : oldsize } );
			
			const fileReadLine = rf.createInterface({
  				input: fsread,
  				crlfDelay: Infinity
			});

			fileReadLine.on('line', (line) => {

			  sio.to(`${socket.id}`).emit('streamingdata', line);
			  
			});
			 
			const getLastSize = ( socketUsers , socket )=>{

				const logFilename = socketUsers[socket.id]['filenamePath'];
			
				return new Promise( ( resolve, reject ) =>{
			
					fs.stat(logFilename,(err,statser)=>{
						let currFileLastSize = statser.size;
						
						if( currFileLastSize == 0 ){
							reject("Failed to read the file");
						}else{
							resolve(currFileLastSize);
						}
					});	
				});
			}
			getLastSize(socketUsers, socket).then( lastsize => {
				socketUsers[socket.id]['location'] = lastsize;
			})
			
		
		});
		}				
	});
}

module.exports = reader;